Y'a des doudous partout, c'est fou
Celle de ma chanson, elle a les yeux vraiment doux
On dirait des cailloux, des perles
Aussi noires que sa peau cachou.

Elle travaille dans un magasin
Elle vend des maillots de bain
A des belles et à des boudins
A des moches et à des biens
A des vieilles qu'ont la peau qui craint
A des jeunes qu'en prennent bien soin
A des pucelles à des putains
Et toute la journée il faut se les fader

Dans son petit magasin
Le soleil n'entre jamais, mais c'est très bien
La doudou elle s'en fout
Au mois d'août elle met les bouts.

La doudou dit bonjour toujours,
Aux dames aux demoiselles
Qui viennent acheter chez elle
Des bikinis pourris très chers
A fleurs ou à rayures panthère
'l'est polie avec les mémères
Qui mérit'raient des beignes
'l'est gentille avec les belles-mères
Elle connaît pas la mienne
La doudou, c'est sans dire un mot
Qu'elle supporte les pauvres têtes
Des pouffiasses de la conso'
Qui croient comme des bêtes que la beauté s'achète
Dans son petit magasin
L'amour n'entre jamais, mais ça n'fait rien
La doudou, elle s'en fout
Au mois d'Août, elle mes les bouts.

La doudou va larguer bientôt
Son tout petit boulot de vendeuse de maillots
Dans un pays plus chaud, plus beau
Elle va aller brûler sa peau.

Dans son île sous les cocotiers
Où elle est la plus belle
Le soleil, l'amour, le reggae
Vont bientôt s'occuper d'elle
De son corps bronzé tout entier
Sans la marque du maillot
La doudou n'en porte jamais
Elle dit : ce truc idiot, c'est bon pour les cageots.

Dans son petit magasin
La doudou trouve qu'il est long le mois de juin
La doudou, elle s'en fout
Au mois d'Août, elle met les bouts.

La doudou elle s'en fout
Au mois d'août elle met les bouts.