import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show SystemChrome, rootBundle;
import 'package:html_character_entities/html_character_entities.dart';
import 'package:wakelock/wakelock.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setEnabledSystemUIOverlays ([]);
  Wakelock.enable();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
      brightness: Brightness.dark,
        primarySwatch: Colors.deepOrange,
        accentColor: Colors.grey.shade900,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _showSongs = false;
  String _category = 'none';
  String _song = 'none';
  String _songLyrics = 'Choose a song';
  List<Widget> _songs = [];

  Future _loadSongs(String category) async {
    String songsData = await rootBundle.loadString('AssetManifest.json');
    Map<String, dynamic> songs = jsonDecode(songsData);
    List<String> songsFromCategory = songs.keys
      .where((e) => e.contains(category))
      .map((e) => e.split('/').last.split('.').first)
      .toList();

    setState(() {
      _category = category;
      _songs = songsFromCategory.map((e) => SongTile(HtmlCharacterEntities.decode(e).replaceAll('%20', ' '), _song, _loadLyrics)).toList();
      _showSongs = true;
    });
  }

  Future _loadLyrics(String song) async {
    String newLyrics = await rootBundle.loadString('assets/songs/$_category/$song.txt');
    setState(() {
      _song = song;
      _songLyrics = newLyrics;
      _showSongs = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    List<Widget> categories = [
      CategoryTile('Wriggles', _category, _loadSongs),
      CategoryTile('Tryo', _category, _loadSongs),
      CategoryTile('Renaud', _category, _loadSongs),
      CategoryTile('Chanson_francaise', _category, _loadSongs),
      CategoryTile('Dessins_animes', _category, _loadSongs),
      CategoryTile('Animes', _category, _loadSongs),
      CategoryTile('Cocomelon', _category, _loadSongs),
      CategoryTile('Autres', _category, _loadSongs),
    ];

    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: 200,
              child: GridView.count(
                childAspectRatio: 4,
                crossAxisCount: 2,
                children: categories
              )
            ),
            Expanded(
              child: _showSongs
              ? ListView(children: _songs)
              : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Text(_songLyrics, style: TextStyle(height: 1.4, color: Colors.grey.shade700, fontSize: 20))
                )
              )
            ),
          ],
        ),
      ),
    );
  }
}

class CategoryTile extends StatelessWidget {
  const CategoryTile(this.category, this.selectedCategory, this.loader, {Key key}) : super(key: key);
  final String category;
  final String selectedCategory;
  final Function loader;

  @override
  Widget build(BuildContext context) {
    print(selectedCategory);
    return GestureDetector(
      onTap: () => loader(category),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.shade900),
          color: category == selectedCategory ? Colors.grey.shade900 : null
        ),
        child: Center(child: Text(category, style: TextStyle(color: Colors.grey.shade700, fontSize: 16))),
      ),
    );
  }
}

class SongTile extends StatelessWidget {
  const SongTile(this.song, this.selectedSong, this.loader, {Key key}) : super(key: key);
  final String song;
  final String selectedSong;
  final Function loader;

  @override
  Widget build(BuildContext context) {
    print(selectedSong);
    return GestureDetector(
      onTap: () => loader(song),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        margin: EdgeInsets.symmetric(vertical: 6),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.shade900),
          color: song == selectedSong ? Colors.grey.shade900 : null
        ),
        child: Center(child: Text(song, style: TextStyle(color: Colors.grey.shade700, fontSize: 16))),
      ),
    );
  }
}