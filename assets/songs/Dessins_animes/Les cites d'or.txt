Enfant du Soleil,
Tu parcours la Terre, le Ciel,
Cherches ton chemin,
C'est ta vie, c'est ton destin.

Et le jour, la nuit,
Avec tes deux meilleurs amis,
A bord du Grand Condor,
Tu recherches les Cités d'Or.

Ah, ah, ah, ah, ah,
Esteban, Zia, Tao, les Cités d'Or
Ah, ah, ah, ah, ah,
Esteban, Zia, Tao, les Cités d'Or

Les Cités d'Or, les Cités d'Or

Enfant du soleil,
Ton destin est sans pareil,
L'aventure t'appelles,
N'attends pas et cours vers elle.

Ah, ah, ah, ah, ah,
Esteban, Zia, Tao, les Cités d'Or.